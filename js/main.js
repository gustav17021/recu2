

function Generar() {
    
    var edad=generarEdad(18,99);
    var altura=(generarAltura(15,25)/10);
    var peso=generarPeso(20,130);

    document.getElementById("edad").value=edad.toFixed(0);
    document.getElementById("altura").value=altura.toFixed(2);
    document.getElementById("peso").value=peso.toFixed(1);
}
function Calcular() {
    let ed=document.getElementById("edad").value
    if(ed==""){
        alert("No se ha generado correctamente")
    }
    else{
        var p=document.getElementById("peso").value
        var a=document.getElementById("altura").value
        var imc=(p/Math.pow(a,2));
        document.getElementById("imc").value=imc.toFixed(2)
        evaluar(imc.toFixed(2));
    }
    
}
function evaluar(imc) {
    if (imc<18.5) {
        document.getElementById("nivel").value="Bajo peso"
    }
    else if(imc>=18.5 && imc<=24.9){
        document.getElementById("nivel").value="Peso saludable"
    }
    else if(imc>=25 && imc<=29.9){
        document.getElementById("nivel").value="Sobrepeso"
    }
    else if(imc>=30.0){
        document.getElementById("nivel").value="Obesidad"
    }
}
var registros=0;
var imcacu=0;
function Registrar() {
    
    let regi=document.getElementById('registros');
    let aux;
    let e=document.getElementById("edad").value
    let p=document.getElementById("peso").value
    let a=document.getElementById("altura").value
    let imc=document.getElementById("imc").value
    let niv=document.getElementById("nivel").value
    
    if(imc==""||e==""){
        alert("No se ha generado o calculado correctamente")
    }
    else{
        imcacu=parseFloat(imc)+imcacu;
        registros=registros+1;
        var prom=Number(imcacu/registros);
        console.log(prom)
        
        aux="Num: "+registros+"  || "+"Edad: "+e+"  || "+"Altura: "+a+"  || "+"Peso: "+p+"  || "+"IMC: "+imc+"  || "+"Nivel: "+niv+"  || "+"<br>";
        document.getElementById("final").value=prom.toFixed(2);
        regi.innerHTML=regi.innerHTML+aux;
        limpiar();
    }
    
}
function promedio(pro) {
   var x=document.getElementById("final").value;
    x=x+pro
    document.getElementById("final").value=x;
}

function generarEdad(min,max) {
    let paso1 = max - min + 1;
    let paso2 = Math.random() * paso1;
    let r = Math.floor(paso2) + min;
    return r;
}
function generarAltura(min,max) {
    let paso1 = max - min + 1;
    let paso2 = Math.random() * paso1;
    let r = paso2+min
    return r;
}
function generarPeso(min,max) {
    let paso1 = max - min + 1;
    let paso2 = Math.random() * paso1;
    let r = Math.floor(paso2) + min;
    return r;
}
function Borrar() {
    registros=0;
    document.getElementById("edad").value   =""
    document.getElementById("peso").value=""
    document.getElementById("altura").value=""
    document.getElementById("imc").value=""
    document.getElementById("nivel").value=""
    document.getElementById('registros').innerHTML="";
    document.getElementById('final').value="";
}
function limpiar(){
    document.getElementById("edad").value   =""
    document.getElementById("peso").value=""
    document.getElementById("altura").value=""
    document.getElementById("imc").value=""
    document.getElementById("nivel").value=""
}